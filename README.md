# Canvas Lighting Demo

This demo contains an example of how to make a lightmap and use that to create lighting over a 2D environment.

## Controls

You can use the sliders to change the light of the color or its position.

![Application Screenshot](/readme-resources/app-screenshot.png "Application Screenshot")