window.addEventListener('load', () => {
  console.log("Initializng...");
  
  let canvas = document.getElementById('the-canvas');

  app = new App(canvas);
  app.start();

  console.log('App started');

  let lightColorRedControl = document.getElementById('light-color-r');
  let lightColorGreenControl = document.getElementById('light-color-g');
  let lightColorBlueControl = document.getElementById('light-color-b');
  let lightColorAlphaControl = document.getElementById('light-color-a');
  let lightPosXControl = document.getElementById('light-pos-x');
  let lightPosYControl = document.getElementById('light-pos-y');

  lightColorRedControl.addEventListener('input', (event) => {
    app.lightColorRed = event.target.value;
  });
  lightColorGreenControl.addEventListener('input', (event) => {
    app.lightColorGreen = event.target.value;
  });
  lightColorBlueControl.addEventListener('input', (event) => {
    app.lightColorBlue = event.target.value;
  });
  lightColorAlphaControl.addEventListener('input', (event) => {
    app.lightColorAlpha = event.target.value;
  });
  lightPosXControl.addEventListener('input', (event) => {
    app.lightPosX = event.target.value;
  });
  lightPosYControl.addEventListener('input', (event) => {
    app.lightPosY = event.target.value;
  });
  console.log('Input listeners created');
});

class App {

  constructor(canvas) {
    this.lightColorRed = 255;
    this.lightColorGreen = 255;
    this.lightColorBlue = 255;
    this.lightColorAlpha = 255;
    this.lightPosX = 100;
    this.lightPosY = 100;

    this.context = canvas.getContext('2d');

    // Create two buffers
    // - base image
    // - lightmap
    this.bufferCanvas = this._createCanvas();
    this.bufferContext = this._createBuffer(this.bufferCanvas);
    this.bufferCanvasLightmap = this._createCanvas();
    this.bufferContextLightmap = this._createBuffer(this.bufferCanvasLightmap);
  }

  start() {
    window.requestAnimationFrame((time) => this._render(time));
  }

  _createCanvas() {
    let canvas = document.createElement('canvas');
    canvas.width = 250;
    canvas.height = 250;
    return canvas;
  }

  _createBuffer(canvas) {
    return canvas.getContext('2d');
  }

  _render(time) {
    console.log('Starting render');
    this._clearCanvas();
    this._renderImage();
    this._renderLighting();
    this._renderToScreen();

    window.requestAnimationFrame((time) => this._render(time));
  }

  _clearCanvas() {
    this.bufferContext.clearRect(0, 0, this.bufferCanvas.width, this.bufferCanvas.height);
    // A 'cleared' lightmap is black because there is no light
    this.bufferContextLightmap.fillStyle = 'black';
    this.bufferContextLightmap.fillRect(0, 0, this.bufferCanvasLightmap.width, this.bufferCanvasLightmap.height);
  }

  _renderImage() {

    this.bufferContext.fillStyle = 'green';
    this.bufferContext.fillRect(0, 0, 50, 50);
    this.bufferContext.fillRect(50, 50, 50, 50);
    this.bufferContext.fillRect(100, 100, 50, 50);

    this.bufferContext.fillStyle = 'red';
    this.bufferContext.fillRect(50, 0, 50, 50);
    this.bufferContext.fillRect(100, 50, 50, 50);
    this.bufferContext.fillRect(150, 100, 50, 50);

    this.bufferContext.fillStyle = 'yellow';
    this.bufferContext.fillRect(100, 0, 50, 50);
    this.bufferContext.fillRect(150, 50, 50, 50);
    this.bufferContext.fillRect(200, 100, 50, 50);
  }

  _renderLighting() {
    var gradient = this.bufferContextLightmap.createRadialGradient(
      this.lightPosX,
      this.lightPosY,
      0,
      this.lightPosX,
      this.lightPosY,
      200);

    var color = ColorUtil.rgba(this.lightColorRed,
      this.lightColorGreen,
      this.lightColorBlue,
      this.lightColorAlpha);

    gradient.addColorStop(0, color);
    gradient.addColorStop(1, 'transparent');
    this.bufferContextLightmap.fillStyle = gradient;
    this.bufferContextLightmap.fillRect(0, 0, this.bufferCanvasLightmap.width, this.bufferCanvasLightmap.height);

    // Multiply values to darken where the light map is
    this.bufferContext.globalCompositeOperation = 'multiply';
    this.bufferContext.drawImage(this.bufferCanvasLightmap, 0, 0);
  }

  _renderToScreen() {
    this.context.drawImage(this.bufferCanvas, 0, 0);
  }
}

class ColorUtil {

  static rgba(red, green, blue, alpha) {
    return '#' + 
      NumberUtil.toHex(red) +
      NumberUtil.toHex(green) +
      NumberUtil.toHex(blue) +
      NumberUtil.toHex(alpha)
  }
}

class NumberUtil {
  static toHex(myString) {
    return Number(myString).toString(16);
  }
}